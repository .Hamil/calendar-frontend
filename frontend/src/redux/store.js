import { createStore } from 'redux';
import calendarReducer from './reducers/calendar';

const store = createStore(calendarReducer);

export default store;