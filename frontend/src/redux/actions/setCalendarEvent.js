export const type = 'setCalendarEvent';

const setCalendarEvent = event => {
    return {
        type,
        payload: event
    }
}

export default setCalendarEvent;