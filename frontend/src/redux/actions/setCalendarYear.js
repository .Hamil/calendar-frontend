export const type = 'setCalendarYear';

const setCalendarYear = year => {
    return {
        type,
        payload: year
    }
}

export default setCalendarYear;