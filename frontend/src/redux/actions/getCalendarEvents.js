export const type = 'getCalendarEvents';

const getCalendarEvents = () => {
    return {
        type,
        payload: []
    }
}

export default getCalendarEvents;