export const type = 'setCalendarMonth';

const setCalendarMonth = event => {
    return {
        type,
        payload: event
    }
}

export default setCalendarMonth;