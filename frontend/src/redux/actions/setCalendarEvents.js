export const type = 'setCalendarEvents';

const setCalendarEvents = events => {
    return {
        type,
        payload: events
    }
}

export default setCalendarEvents;