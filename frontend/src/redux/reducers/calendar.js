const defaultState = { year: new Date().getFullYear(), month: new Date().getMonth(), events: [] };

function reducer(state = defaultState, { type, payload }){
    switch(type){
        case 'setCalendarYear': {
            return {
                ...state,
                year: payload
            }        
        }
        case 'setCalendarMonth': {
            return {
                ...state,
                month: payload
            }        
        }
        case 'setCalendarEvent': {
            return {
                ...state,
                events: [
                    ...state.events,
                    payload
                ]
            }        
        }
        case 'setCalendarEvents': {
            return {
                ...state,
                events: payload
            }        
        }
        default:
            return state;
    }
}

export default reducer;