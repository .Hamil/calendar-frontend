import { Input } from 'reactstrap';

export default function Year({ style, setCalendarYear, year }){
    return(
        <Input type="select" name="year" id="year" style={style} value={year} onChange={e => { setCalendarYear(e.target.value) }}>
          <option value="2021">2021</option>
          <option value="2020">2020</option>
          <option value="2019">2019</option>
          <option value="2018">2018</option>
          <option value="2015">2015</option>
        </Input>
    );
}