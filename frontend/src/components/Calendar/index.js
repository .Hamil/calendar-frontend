import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import "./index.css"
import CalendarPage from "./page";
import BigCalendar from "react-big-calendar";
import { connect } from 'react-redux';
import setCalendarYear from '../../redux/actions/setCalendarYear'
import setCalendarMonth from '../../redux/actions/setCalendarMonth'
import setCalendarEvent from '../../redux/actions/setCalendarEvent'
import setCalendarEvents from '../../redux/actions/setCalendarEvents'
import { useEffect } from "react";
import { useQuery } from '@apollo/client';
import { GET_EVENTS } from '../../graphql/queries';


//Pasandole el objeto/configuracion de MomentJS a Big Calendar
BigCalendar.momentLocalizer(moment);

//obteniendo el array con los parametros de la vista para el componente de Big Calendar
const views = Object
.keys(BigCalendar.Views)
.map(k => BigCalendar.Views[k]);

const CalendarContainer = ({ year, setCalendarYear, month, setCalendarMonth, setCalendarEvent, events, setCalendarEvents }) => {
  //aqui vamos a traer todos nuestros eventos desde el Backend
  const {error, loading, data } = useQuery(GET_EVENTS);
  //el useEffect solo funcionara cuando 'data' sea actualizado
  useEffect(() => {
    if(data){
      setCalendarEvents(data.events);
    }
  }, [data]);
  
    return (
      <CalendarPage
        views={views}
        events={events}
        year={year}
        month={month}
        events={events}
        setCalendarYear={setCalendarYear}
        setCalendarMonth={setCalendarMonth}
        setCalendarEvent={setCalendarEvent}
        dayPropGetter={customDayPropGetter}
        slotPropGetter={customSlotPropGetter}
      />
    );
}

//definiendo el metodo mapStateToProps para traer el estado del store a nuestro componente
const mapStateToProps = state => {
  return {
    year: state.year,
    month: state.month,
    events: state.events
  }
}

//declarando nuestro metodo para decirlo a Redux los dispachers (disparadores de eventos/actions) que va a tener
const mapDispatchToProps =  {
  setCalendarYear,
  setCalendarMonth,
  setCalendarEvent,
  setCalendarEvents
}

//esto es para poder darle formato a los fines de semana
const customDayPropGetter = date => {
  if (date.getDay() === 6 || date.getDay() === 0)
    return {
      className: 'weekend',
      style: {
        background: '#e5e5e5',
      },
    }
  else return {}
}

const customSlotPropGetter = date => {
  if (date.getDay() === 6 || date.getDay() === 0){
    return {
      className: 'weekend',
    }
  }
  else return {}
}

// //devolviendo la conexion del estado
// const wrapper = connect(mapStateToProps);

// //pasando el componente a modificar dentro del wrapper y devolviendo el componente conectado al estado de Redux
// const CalendarContainerConnected = wrapper(CalendarContainer);

//finalmente exportamos nuestro componente conectado al estado de Redux
export default connect(mapStateToProps, mapDispatchToProps)(CalendarContainer);



