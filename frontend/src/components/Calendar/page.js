import AddEvent from './AddEvent/page';
import Year from './Year/page';
import Month from './Month/page';
import BigCalendar from "react-big-calendar";

export default function CalendarPage({ views, setCalendarYear, year, setCalendarMonth, month, setCalendarEvent, events, dayPropGetter, slotPropGetter }){
    return (
        <div className="mainContainer">
          <div className="addEventContainer">
            <div className="buttonAddEventContainer">
                <p className="labelAddEvent">ADD AN EVENT!</p>
                <AddEvent setCalendarEvent={setCalendarEvent}/>
            </div>
          </div>
        <div className="calendarContainer">
            <div className="filtersContainer">
                {/* mostrando los filtros en el contenedor de la parte superior */}
                <Year style={{width: '35%'}} setCalendarYear={setCalendarYear} year={year}/>
                <Month style={{width: '35%'}} setCalendarMonth={setCalendarMonth} month={month}/>
            </div>
            <BigCalendar
            style={{ height: "100vh", width: "100%", backgroundColor: "white" }}
            toolbar={false}
            events={events}
            step={60}
            views={views}
            view={"month"}
            onView={() => {}}
            date={new Date(year, month)}
            dayPropGetter={dayPropGetter}
            slotPropGetter={slotPropGetter}
            />
        </div>
      </div>
    );
}