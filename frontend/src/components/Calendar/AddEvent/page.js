import { useState } from 'react';
import { GrAdd } from 'react-icons/gr';
import { Modal, ModalHeader, ModalBody, Form, Input } from 'reactstrap';
import './index.css';
import { CREATE_EVENT } from '../../../graphql/mutations';
import { useMutation } from '@apollo/client';


export default function AddEventModal({ setCalendarEvent }) {
    const [modal, setModal] = useState(false);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [date, setDate ] = useState('');
    const [ createEvent, { error }] = useMutation(CREATE_EVENT);

    const toggle = () => setModal(!modal);

    const onSubmit = e => {
        //evitando el comportamiento por default
        e.preventDefault();

        //validando que el formulario no este vacio
        if(name !== '' && description !== '' && date !== ''){
            let dateFormat = `${date} 00:00`;

            let event = {
                'title': name,
                'start': new Date(dateFormat),
                'allDay': false,
                'end': new Date(dateFormat),
                'desc': description
            }
            
            if(error){
                console.log(`Hubo un error a la hora de guardar un evento con GraphQL --> ${error}`);
            }else{
                //mandando el evento creado a Redux con nuestro action (esto es solo para hacer render y que se vean nuestros eventos que vienen desde el backend)
                setCalendarEvent(event);
                //usando nuestro mutation para crear el evento en el backend
                createEvent({
                    variables: event
                });
                toggle();
            }
        }

        //limpiando el estado despues de cerrar el modal
        setName('');
        setDescription('');
        setDate('');
    }

    return (
        <div>
            <button className="buttonAddEvent" onClick={toggle}>
                <GrAdd size={50}/>
            </button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader className="modalHeader" toggle={toggle}>Create Event</ModalHeader>
                <ModalBody>
                <Form>
                    <Input placeholder="Name" onChange={e => {setName(e.target.value)} }/>
                    <br/>
                    <Input type="textarea" name="description" id="description" placeholder="Description" onChange={e => {setDescription(e.target.value)}}/>
                    <br/>
                    <Input
                        type="date"
                        name="date"
                        onChange={e => {setDate(e.target.value)}}
                        placeholder="date placeholder"
                    />
                    <div className="modal-footer">
                        <button type="submit" id="buttonModalFooter" className="btn" onClick={onSubmit} >Create</button> 
                        <button type="button" className="btn btn-secondary" onClick={toggle}>Cancel</button>
                    </div>
                </Form>
                </ModalBody>
            </Modal>
            </div>
      );
}