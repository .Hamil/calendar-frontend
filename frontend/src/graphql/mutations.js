import { gql } from '@apollo/client';

export const CREATE_EVENT = gql`
    mutation createEvent(
        $title: String!
        $desc: String! 
        $allDay: Boolean! 
        $start: Date!
        $end: Date!
        )
    {
    createEvent(
            title: $title 
            desc: $desc
            allDay: $allDay 
            start: $start
            end: $end
        )
        {
            id
            title
            start
            end
        }
    }
`