import { gql } from '@apollo/client';

export const GET_EVENTS = gql`
    query {
        events{
            id
            title
            desc
            allDay
            start
            end
        }
    }
`