import Calendar from "./components/Calendar";
import './App.css';
import { ApolloClient, InMemoryCache, ApolloProvider, HttpLink, from} from '@apollo/client';
import { onError } from '@apollo/client/link/error';

//condicionando lo que debe pasar cuando ocurra un error
const errorLink = onError(({ graphqlErrors, networkError }) => {
  if(graphqlErrors){
    graphqlErrors.map(({ message, location, path }) => {
      console.log(`GraphQL tiene un error --> ${message}`);
    })
  } else if(networkError){
    console.log(`Error de red`);
  }
})

//configurando el enlace y los posibles errores
const link = from([
  errorLink,
  new HttpLink({uri: "/graphql"})
])

//configurando el cliente
const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link
})

function App() {
  return (
    //wrapping nuestro componente con el provider de apollo
    <ApolloProvider client={client}>
      <div className="App">
        <Calendar/>
      </div>
    </ApolloProvider>
  );
}

export default App;
